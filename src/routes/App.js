import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import './App.css';
import Layout from "../components/Layout";
import NotFound from "../components/NotFound";
import Characters from '../pages/Characters';
import CharDetail from '../pages/CharDetail';
import Store from "../context/Store";


function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Store>
          <Layout>
            <Switch>
              <Route exact path="/" component={Characters} />
              <Route exact path="/detail/" component={CharDetail} />
              <Route component={NotFound} />
            </Switch>
          </Layout>
        </Store>
      </div>
    </BrowserRouter>
  );
}

export default App;
