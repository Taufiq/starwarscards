import fetch from "isomorphic-fetch";

const urlChars = "https://swapi.dev/api/people/";
const urlFilms = "https://swapi.dev/api/films/";
//console.log(url);

const headers = new Headers({
    "Content-Type": "application/json",
    Accept: "application/json"
});

const swapi = {
  characters: {
    getChar: async (payload) => {
      console.log(`in swapi.js getting char: ${payload}`);
      const datos = await fetch(`${urlChars}/${payload.idChar}`,{
          method:'GET',
          headers,
          body: JSON.stringify(payload)
      })
        .then((datos) => datos.json() )
        .catch((error) => {
          throw new Error(error);
        });
      console.log("result: ", datos);
      return datos;
    },
    
    getCharsList: async (page)=>{
        console.log(`${urlChars}`);
        const datos = await fetch(`${urlChars}?page=`+page,{
            method: 'GET',
            headers,
        }).then(async (result)=>{
            if (result.ok) {
              const datos = await result.json();
              return { success: true, data: datos };
            } else {
              return { success: false, msg: "Unauthorized" };
            }
        })
        .catch((error) => {
            throw new Error(error);
          });
        return datos;
    }
  },
  films:{
    getDataMovies : async () =>{
      console.log("Getting movies from url: "+ urlFilms)
      const datos = await fetch(`${urlFilms}`,{
        method:'GET',
        headers,
      })
      .then(async (result)=>{
        if (result.ok) {
          const datos = await result.json();
          return { success: true, data: datos };
        } else {
          return { success: false, msg: "Unauthorized" };
        }
      })
      .catch((error) => {
         throw new Error(error);
      });
    return datos;
    }
  }
};

export default swapi;
