
import CreativeCard from '../components/CreativeCard';
import SearchBar from '../components/SearchBar'
import { Grid } from "@material-ui/core";
import { makeStyles } from '@material-ui/core/styles';
import { useContext, useEffect, useState } from 'react';
import swapi from '../api/swapi';
import { Context } from '../context/Store';
import PaginationButtons from '../components/Pagination';


const useStyles = makeStyles((theme) => ({
    gridContainer:{
        paddingLeft: "2em",
        paddingRight: "2em",
    },
    root: {
        marginBottom: '30px',
        padding: '2px 4px',
        display: 'flex',
        alignItems: 'center',
        maxWidth: 600,
      },
      input: {
        marginLeft: theme.spacing(1),
        flex: 1,
      },
      iconButton: {
        padding: 8,
      },
      divider: {
        height: 28,
        margin: 4,
      },
}));

const Characters = (props) => {
    const [state, dispatch] = useContext(Context);

    console.log(" Page :",state.page);
    
    const loadChars = async (pag) => {
        const response = await swapi.characters.getCharsList(pag);
        if(response.success){
            dispatch({type:'SET_CHARS',payload: response.data.results });
        }else{
            dispatch({type:'SET_ERROR',payload: "error" });
        }
    };
   
    const classes = useStyles();
    
    useEffect(() => {
        loadChars(state.page);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);
    
    const charList = state.filterList;
    return (
      <div className="App" >
        <Grid container spacing={5} className={classes.gridContainer}
                justify={"center"}>
            <SearchBar></SearchBar>
        </Grid>
       
        <Grid container spacing={5} className={classes.gridContainer}
                justify={"center"}>

            {charList.map((char,index) =>(
                <Grid item xs={12} sm={6} md={4} key={index}>
                    <CreativeCard char={char}></CreativeCard>
                </Grid>
            ))}

        </Grid>
        <Grid container spacing={5} className={classes.gridContainer}
                justify={"center"}>
            <PaginationButtons loadNewChars={loadChars}></PaginationButtons>      
        </Grid>
      </div> 
    );
}


export default Characters;