import React, { useContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Pagination from '@material-ui/lab/Pagination';
import { Typography } from '@material-ui/core';
import { Context } from '../context/Store';

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      marginTop: theme.spacing(2),
    },
  },
}));

export default function PaginationButtons(props) {
  const classes = useStyles();
  
  const [page, setPage] = React.useState(1);
  const [state, dispatch] = useContext(Context);

  const handleChange = (event, value) => {
    setPage(value);
    dispatch({type:'PAGINATOR',payload: value });
    props.loadNewChars(value);
   
  };

  return (
    <div className={classes.root}>
      <Typography>Page: {page}</Typography>
      <Pagination count={10} showFirstButton showLastButton 
        onChange={handleChange}/>
    </div>
  );
}