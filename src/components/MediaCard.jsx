import React, { useContext } from 'react';
import { Context } from '../context/Store';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ShareIcon from '@material-ui/icons/Share';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import { Grid } from '@material-ui/core';
import { useHistory } from "react-router-dom";
import CreativeCard from './CreativeCard';
//import bg from  '../assets/static/img/stars-bg.png';
//import { FlipCameraAndroid } from '@material-ui/icons';

const useStyles = makeStyles((theme) => ({
  root: {
    margin:40,
    padding: 40,
    maxWidth: 600,
  },
  grid:{
    backgroundColor: 'gray'
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    backgroundColor: red[500],
  },
  textDetail:{
    float: 'right',
    textAlign: 'right'
  }
}));

const MediaCard = () => {
  const history = useHistory();
  const [state, dispatch] = useContext(Context);

  console.log("SelectedChar :",state.selectedChar)
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };
  
  let idCharMovies = state.selectedChar.films.map(film=> film.split("/")[5]);
  console.log("Printing idCharMovies ",idCharMovies);

  return (
    <>
    <div className={classes.textDetail}>
      <IconButton onClick={history.goBack}>Back</IconButton>
    </div>
    <Grid container direction="row"
      justify="center" alignItems="center"
      className={classes.grid}
    >
      <Card className={classes.root}>
        <CardHeader
          avatar={
            <Avatar aria-label="recipe" className={classes.avatar}>
              {state.selectedChar.name[0]}
            </Avatar>
          }
          action={
            <IconButton aria-label="settings">
              <MoreVertIcon />
            </IconButton>
          }
          title={state.selectedChar.name}
          subheader={`Birth: ${state.selectedChar.birth_year}`}
        />
        <CardMedia
          className={classes.media}
          image={state.imgChar}
          title="Paella dish"
        />
        <CardContent>
            { state.films.map((film,index) => {   
                  if(idCharMovies.includes(film.episode_id.toString())){
                    console.log("Match film: ",film); 
                    <>
                      <Typography variant="body2" color="textSecondary" component="p">Pelicula</Typography>
                      <Grid item xs={12} sm={6} md={4} key={index}>
                        <CreativeCard>hola</CreativeCard>
                      </Grid>
                    </>
                  }
                })
              };
          <Typography variant="body2" color="textSecondary" component="p">
            This impressive paella is a perfect party dish and a fun meal to cook together with your
            guests. Add 1 cup of frozen peas along with the mussels, if you like.
          </Typography>
        </CardContent>
        <CardActions disableSpacing>
          <IconButton aria-label="add to favorites">
            <FavoriteIcon />
          </IconButton>
          <IconButton aria-label="share">
            <ShareIcon />
          </IconButton>
          
          <IconButton
            className={clsx(classes.expand, {
              [classes.expandOpen]: expanded,
            })}
            onClick={handleExpandClick}
            aria-expanded={expanded}
            aria-label="show more"
          >
            <ExpandMoreIcon />
          </IconButton>
          <div className={classes.textDetail}>
            <p >More Info</p>
          </div>
        </CardActions>
        <Collapse in={expanded} timeout="auto" unmountOnExit>
          <CardContent>
            <Typography paragraph>Movies:</Typography>
              hola
            <Typography paragraph>
              Heat oil in a (14- to 16-inch) paella pan or a large, deep skillet over medium-high
              heat. Add chicken, shrimp and chorizo, and cook, stirring occasionally until lightly
              browned, 6 to 8 minutes. Transfer shrimp to a large plate and set aside, leaving chicken
              and chorizo in the pan. Add pimentón, bay leaves, garlic, tomatoes, onion, salt and
              pepper, and cook, stirring often until thickened and fragrant, about 10 minutes. Add
              saffron broth and remaining 4 1/2 cups chicken broth; bring to a boil.
            </Typography>
            <Typography paragraph>
              Add rice and stir very gently to distribute. Top with artichokes and peppers, and cook
              without stirring, until most of the liquid is absorbed, 15 to 18 minutes. Reduce heat to
              medium-low, add reserved shrimp and mussels, tucking them down into the rice, and cook
              again without stirring, until mussels have opened and rice is just tender, 5 to 7
              minutes more. (Discard any mussels that don’t open.)
            </Typography>
            <Typography>
              Set aside off of the heat to let rest for 10 minutes, and then serve.
            </Typography>
          </CardContent>
        </Collapse>
      </Card>
    </Grid>
  </>
  );
}


export default MediaCard;