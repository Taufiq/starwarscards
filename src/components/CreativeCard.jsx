import React, { useContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import cx from 'clsx';
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import TextInfoContent from '@mui-treasury/components/content/textInfo';
import { useBlogTextInfoContentStyles } from '@mui-treasury/styles/textInfoContent/blog';
import { useOverShadowStyles } from '@mui-treasury/styles/shadow/over';
import { useHistory } from "react-router-dom";
import { Context } from '../context/Store';
import swapi from '../api/swapi';

const useStyles = makeStyles(({ breakpoints, spacing }) => ({
  root: {
    margin: 'auto',
    borderRadius: spacing(2), // 16px
    transition: '0.3s',
    boxShadow: '0px 14px 80px rgba(34, 35, 58, 0.2)',
    position: 'relative',
    maxWidth: 400,
    marginLeft: 'auto',
    overflow: 'initial',
    background: '#ffffff',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    paddingBottom: spacing(2),
    [breakpoints.up('md')]: {
      flexDirection: 'row',
      paddingTop: spacing(2),
    },
  },
  media: {
    width: '70%',
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: spacing(-3),
    height: 0,
    paddingBottom: '48%',
    borderRadius: spacing(2),
    backgroundColor: '#fff',
    position: 'relative',
    [breakpoints.up('md')]: {
      width: '100%',
      marginLeft: spacing(-3),
      marginTop: 0,
      transform: 'translateX(-8px)',
    },
    '&:after': {
      content: '" "',
      position: 'absolute',
      top: 0,
      left: 0,
      width: '100%',
      height: '100%',
      backgroundImage: 'linear-gradient(147deg, #fe8a39 0%, #fd3838 74%)',
      borderRadius: spacing(2), // 16
      opacity: 0.5,
    },
  },
  content: {
    padding: 24,
  },
  cta: {
    marginTop: 24,
    textTransform: 'initial',
  },
}));

const getId = (urlChar)=>{
  const array = urlChar.split("/");
  //console.log("Array ",array);
  return array[5];
}



export const CreativeCard = React.memo(function BlogCard(props) {

  const [state, dispatch] = useContext(Context);

  const styles = useStyles();
  const {
    button: buttonStyles,
    ...contentStyles
  } = useBlogTextInfoContentStyles();

  const history = useHistory();
  const imgChar =`https://starwars-visualguide.com/assets/img/characters/${getId(props.char.url)}.jpg`;

  const routeChange = () =>{
    getFilms();
    dispatch({type:'SET_CHAR',payload: { dataChar: props.char, img: imgChar}});
    history.push({
      pathname: `/detail`,
      state:{char: props.char}
    });
  }

  const getFilms= async ()=>{
    const response = await swapi.films.getDataMovies();
    if(response.success){
        dispatch({type:'SET_FILMS',payload: response.data.results });
    }else{
        dispatch({type:'SET_ERROR',payload: "error" });
    }
  }

  const shadowStyles = useOverShadowStyles();
  return (
    <Card className={cx(styles.root, shadowStyles.root)}>
      <CardMedia
        className={styles.media}
        image={imgChar}      
      />
      <CardContent>
        <TextInfoContent
          classes={contentStyles}
          overline={`Birth : ${props.char.birth_year}`}
          heading={props.char.name}
          body={`Gender : ${props.char.gender} `}
        />
        <Button onClick={routeChange} className={buttonStyles}>Read more</Button>
      </CardContent>
    </Card>
  );
});

export default CreativeCard