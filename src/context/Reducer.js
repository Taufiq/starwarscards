const Reducer = (state, action) => {
    switch (action.type) {
        case 'SET_CHARS':
            return {
                ...state,
                charList: action.payload,
                filterList: action.payload,
            };
        case 'SET_CHAR':
            return {
                ...state,
                selectedChar: action.payload.dataChar,
                imgChar: action.payload.img
            };
        case 'PAGINATOR':
            return {
                ...state,
                page: action.payload,
            };
        case 'FILTER_CHARS':
            return {
                ...state,
                filterList: state.charList.filter( (char) => 
                            char.name.toLowerCase().includes(action.payload.toLowerCase())
                            )
            };
        case 'SET_FILMS':
            return {
                ...state,
                films: action.payload
            };
        case 'SET_ERROR':
            return {
                ...state,
                error: action.payload
            };
        default:
            return state;
    }
};

export default Reducer;